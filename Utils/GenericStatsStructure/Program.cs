﻿using GenericStatsSystem.Samples;

namespace GenericStats
{
    class Program
    {
        static void Main(string[] args)
        {
            SomeBasicStatsUsage.Run();
            InheritedStats.Run();
            UnitTestingDoneWrong.Run();
        }
    }
}
