using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GenericStatsSystem.Samples
{
    /// <summary>
    /// Class used ro tun some tests
    /// </summary>
    public class UnitTestingDoneWrong
    {
        /// <summary>
        /// Debug function thats write some equality information about two Stats<TKeyType>
        /// </summary>
        /// <typeparam name="TKeyType"></typeparam>
        /// <param name="pStats1"></param>
        /// <param name="pStats2"></param>
        static void WriteEquality<TKeyType>(Stats<TKeyType> pStats1, Stats<TKeyType> pStats2)
        {
            Console.WriteLine(" " + ( pStats1 == pStats2 ) + "\t| " + pStats1.Equals(pStats2) + "\t| HashCode = " + ( pStats1 != null ? pStats1.GetHashCode().ToString() : "null" ) + " | " + ( pStats2 != null ? pStats2.GetHashCode().ToString() : "null" ));
        }

        /// <summary>
        /// Runs some basic tests
        /// </summary>
        public static void Run()
        {
            try
            {
                // Let's build a Potion
                Stats<string> potion = new Stats<string>() { { "HITPOINTS", 50 } };
                Console.WriteLine("POTION >> " + potion);

                // Build Superpotion from a Potion
                Stats<string> superPotion = new Stats<string>(potion) { { "MANA", 50 } };
                Console.WriteLine("SUPER POTION >> " + superPotion);

                // Create a player
                Stats<string> player = new Stats<string>();
                // Add
                player.Add("HITPOINTS", 100f);
                Debug.Assert(player.HasStat("HITPOINTS"));
                Debug.Assert(player["HITPOINTS"] == 100f);
                // Auto add if missing
                player["MANA"] = 100f;
                Debug.Assert(player.HasStat("MANA"));
                Debug.Assert(player["MANA"] == 100f);
                // Add with no value -- default value = 0f
                player.AddStat("TO_BE_REMOVED");
                Debug.Assert(player["TO_BE_REMOVED"] == 0f);
                // Removal
                if (player.HasStat("TO_BE_REMOVED"))
                    player.RemoveStat("TO_BE_REMOVED");
                Debug.Assert(!player.HasStat("TO_BE_REMOVED"));

                Console.WriteLine("BASE PLAYER >> " + player);

                // Clear Player
                player.Clear();
                Debug.Assert(player.Count == 0);

                // Add from custom API
                player.AddStat("HITPOINTS", 100f);
                Debug.Assert(player.HasStat("HITPOINTS"));
                Debug.Assert(player["HITPOINTS"] == 100f);
                // Add with existing value -- should not overwrite
                player.AddStat("HITPOINTS", 40f);
                Debug.Assert(player.HasStat("HITPOINTS"));
                Debug.Assert(player["HITPOINTS"] == 100f);

                // Add
                player.SetStat("MANA", 100f);
                Debug.Assert(player.HasStat("MANA"));
                Debug.Assert(player["MANA"] == 100f);
                player.AddStat("TO_BE_REMOVED", 100f);

                if (player.HasStat("TO_BE_REMOVED"))
                    player.RemoveStat("TO_BE_REMOVED");
                Console.WriteLine("BASE PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 100f && player["MANA"] == 100f && !player.HasStat("TO_BE_REMOVED"));

                // add
                Console.WriteLine("################");
                Console.WriteLine("######## ADD ########");

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player += 50;
                Console.WriteLine("PLAYER + 50 >> " + player);
                Debug.Assert(player["HITPOINTS"] == 150f && player["MANA"] == 150f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Add(50);
                Console.WriteLine("PLAYER.Add(50) >> " + player);
                Debug.Assert(player["HITPOINTS"] == 150f && player["MANA"] == 150f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Add(superPotion);
                Console.WriteLine("PLAYER.Add(superPotion) >> " + player);
                Debug.Assert(player["HITPOINTS"] == 150f && player["MANA"] == 150f);

                // sub
                Console.WriteLine("################");
                Console.WriteLine("######## SUB ########");

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player -= 50;
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 50f && player["MANA"] == 50f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Sub(50);
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 50f && player["MANA"] == 50f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Sub(superPotion);
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 50f && player["MANA"] == 50f);

                // Mult
                Console.WriteLine("################");
                Console.WriteLine("######## MULT ########");

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player *= 50;
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 5000f && player["MANA"] == 5000f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Mult(50);
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 5000f && player["MANA"] == 5000f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Mult(superPotion);
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 5000f && player["MANA"] == 5000f);

                // Div
                Console.WriteLine("################");
                Console.WriteLine("######## DIV ########");

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player /= 50;
                Console.WriteLine("PLAYER >> " + player);
                Debug.Assert(player["HITPOINTS"] == 2f && player["MANA"] == 2f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Div(50);
                Console.WriteLine("PLAYER.Div(50) >> " + player);
                Debug.Assert(player["HITPOINTS"] == 2f && player["MANA"] == 2f);

                player.SetStat("HITPOINTS", 100f);
                player.SetStat("MANA", 100f);
                player.Div(superPotion);
                Console.WriteLine("PLAYER.Div(superPotion) >> " + player);
                Debug.Assert(player["HITPOINTS"] == 2f && player["MANA"] == 2f);

                // Hash code tests
                Console.WriteLine("POTION = " + potion.GetHashCode());
                Console.WriteLine("SUPERPOTION = " + superPotion.GetHashCode());
                Console.WriteLine("PLAYER = " + player.GetHashCode());
                Debug.Assert(potion.GetHashCode() != superPotion.GetHashCode());
                Debug.Assert(player.GetHashCode() != superPotion.GetHashCode());

                player.Clear();
                foreach (KeyValuePair<string, float> pair in superPotion)
                    player.SetStat(pair.Key, pair.Value);
                Console.WriteLine("COPIED SUPERPOTION VALUES INTO PLAYER = " + player.GetHashCode());
                Debug.Assert(player.GetHashCode() == superPotion.GetHashCode());

                player.Clear();
                player.SetStat("MANA", 50f);
                player.SetStat("HITPOINTS", 50f);
                Console.WriteLine("ADDED SUPERPOTION VALUES INTO PLAYER = " + player.GetHashCode());
                Debug.Assert(player.GetHashCode() == superPotion.GetHashCode());

                player.SetStat("MANA", 50.0f);
                player.SetStat("HITPOINTS", 50.00001f);
                Debug.Assert(player.GetHashCode() != superPotion.GetHashCode());

                // Equality Tests

                Stats<string> stats1 = new Stats<string>();
                Stats<string> stats2 = new Stats<string>();

                WriteEquality(stats1, null);
                Debug.Assert(stats1 != null);
                WriteEquality(stats1, stats1);
                Debug.Assert(stats1 == stats1);
                WriteEquality(stats1, stats2);
                Debug.Assert(stats1 == stats2);

                stats1.AddStat("test", 1);
                WriteEquality(stats1, stats2);
                Debug.Assert(!( stats1 == stats2 ));
                Debug.Assert(stats1 != stats2);

                stats2.AddStat("test", 1);
                WriteEquality(stats1, stats2);
                Debug.Assert(stats1 == stats2);
                Debug.Assert(!( stats1 != stats2 ));

                stats2.SetStat("test", 2);
                WriteEquality(stats1, stats2);
                Debug.Assert(!( stats1 == stats2 ));
                Debug.Assert(stats1 != stats2);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error >> " + e);
            }
        }
    }
}
