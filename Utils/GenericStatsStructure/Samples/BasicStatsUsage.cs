﻿using System;
using System.Collections.Generic;

namespace GenericStatsSystem.Samples
{
    /// <summary>
    /// Helper for storing stats names as strings. When using string as key, it avoids typing errors.
    /// </summary>
    public static class StatsStringHelper
    {
        public const string AGILITY = "agi";
        public const string HITPOINTS = "hp";
        public const string SPEED = "sp";
    }

    /// <summary>
    /// Class used ro tun some tests
    /// </summary>
    public class SomeBasicStatsUsage
    {
        /// <summary>
        /// Run some basic tests
        /// </summary>
        public static void Run()
        {
            // Declaration -- choose your key type
            Stats<string> myStats = new Stats<string>();

            #region Using stats as a dictionary
            Console.WriteLine("##### Using stats as a dictionary ####");
            myStats.Clear();
            // Adding stats
            myStats.Add("str", 20f);
            myStats.Add(StatsStringHelper.HITPOINTS, 30f);
            myStats.Add(new KeyValuePair<string, float>(StatsStringHelper.AGILITY, 20f));
            // Setting value of existing stats
            myStats["str"] = 10f;
            // Checking stats presence
            if (!myStats.ContainsKey(StatsStringHelper.SPEED))
                myStats.Add(StatsStringHelper.SPEED, -5f);
            // Removing stats
            myStats.Remove(StatsStringHelper.SPEED);
            // Displaying Stats -- Using custom ToString for easy lookups
            Console.WriteLine(myStats);
            Console.WriteLine("#####################");
            #endregion

            #region  Using stats with custom API -- Use those methods to add specific behaviors
            Console.WriteLine("##### Using stats with custom API ####");
            // Adding stats -- only adds if the no previous value were found
            myStats.AddStat("str", 20f);
            myStats.AddStat(StatsStringHelper.AGILITY, 20f);
            // Setting stats values -- automaticaly adds the value if missing
            myStats.SetStat("str", 10f);
            myStats.SetStat(StatsStringHelper.HITPOINTS, 30f);
            // Checking stats presence -- same as ContainsKey but could be enhanced with value cheking and/or predicates
            if (!myStats.HasStat(StatsStringHelper.SPEED))
                myStats.AddStat(StatsStringHelper.SPEED, -5f);
            // Removing stats
            myStats.RemoveStat(StatsStringHelper.SPEED);
            // Displaying stats
            Console.WriteLine(myStats);
            Console.WriteLine("#####################");
            #endregion

            #region stats operations
            // Applying a numerical bonus to multiple fields
            Console.WriteLine("##### Applying a numerical bonus to all fields #####");
            Console.WriteLine("Base Values = " + myStats);
            Console.WriteLine("all fields + 2f =" + (myStats + 2f));
            Console.WriteLine("all fields - 2f =" + (myStats - 2f));
            Console.WriteLine("all fields * 2f =" + (myStats * 2f));
            Console.WriteLine("all fields / 2f =" + (myStats / 2f));
            Console.WriteLine("#####################");

            // Stats as a bonus
            Console.WriteLine("##### Using stats as a bonus to apply #####");
            Stats<string> bonus = new Stats<string>();
            bonus.AddStat("str", 10f);
            bonus.AddStat(StatsStringHelper.HITPOINTS, -10f);
            Console.WriteLine("Base Values = " + myStats);
            Console.WriteLine("Bonus Values = " + bonus);
            Console.WriteLine("Base + bonus =" + (myStats + bonus));
            Console.WriteLine("Base - bonus =" + (myStats - bonus));
            Console.WriteLine("Base * bonus =" + (myStats * bonus));
            Console.WriteLine("Base / bonus =" + (myStats / bonus));
            Console.WriteLine("#####################");
            #endregion
        }
    }
}
