﻿using System;

namespace GenericStatsSystem.Samples
{
    /// <summary>
    /// Helper for storing stats names as enum.
    /// </summary>
    public enum StatsNameHelper
    {
        STRENGTH,
        AGILITY,
        HITPOINTS,
        SPEED,
    }

    /// <summary>
    /// List of operators that can be translated to a Stats operation
    /// </summary>
    public enum Operator
    {
        Add,
        Sub,
        Mult,
        Div
    }

    /// <summary>
    /// Generic potion
    /// </summary>
    public class ActivableItem : Stats<StatsNameHelper>
    {
        public Operator pOperator = Operator.Add;

        public bool IsInCooldown()
        {
            return false;
        }
    }

    /// <summary>
    /// Generic entity that inherited from Stats class, using enum for keying stats
    /// </summary>
    public class GenericEntity : Stats<StatsNameHelper>
    {
        // You core classes can easyly inherit from Stats class
        public bool AnyProperty { get; set; }
        public void AnyMethod() { }

        /// <summary>
        /// Apply <paramref name="pItem" /> to self
        /// </summary>
        /// <param name="pItem"></param>
        /// <param name="pOperator"></param>
        public void ApplyItemEffect(ActivableItem pItem)
        {
            switch (pItem.pOperator)
            {
                case Operator.Add:
                    Add(pItem);
                    break;
                case Operator.Sub:
                    Sub(pItem);
                    break;
                case Operator.Mult:
                    Mult(pItem);
                    break;
                case Operator.Div:
                    Div(pItem);
                    break;
                default:
                    break;
            }
        }

        public void TakeDamage(float pAmount)
        {
            SetStat(StatsNameHelper.HITPOINTS, this[StatsNameHelper.HITPOINTS] - pAmount);
            Console.WriteLine("Aouch ! (hitpoints = " + this[StatsNameHelper.HITPOINTS] + ")");
        }
    }

    /// <summary>
    /// Class used to run some tests
    /// </summary>
    class InheritedStats
    {
        /// <summary>
        /// Run some tests using entity as a stat
        /// </summary>
        public static void Run()
        {
            // Declaration -- choose your key type
            GenericEntity player = new GenericEntity();
            player.SetStat(StatsNameHelper.HITPOINTS, 100f);
            player.SetStat(StatsNameHelper.STRENGTH, 100f);
            player.SetStat(StatsNameHelper.AGILITY, 100f);
            Console.WriteLine("BASE PLAYER =" + player);

            ActivableItem healingPotion = new ActivableItem();
            healingPotion.SetStat(StatsNameHelper.HITPOINTS, 50f);
            Console.WriteLine("HEALING POTION =" + player);

            // A fight happens -- player takes damage
            player.TakeDamage(15f);
            player.TakeDamage(55f);

            // Player drinks potion
            Console.WriteLine("I must heal !");
            if (!healingPotion.IsInCooldown())
                player.ApplyItemEffect(healingPotion);
            Console.WriteLine("*drinking potion* aaaaah ! (+" + healingPotion[StatsNameHelper.HITPOINTS] + ")");

            // Displaying stats
            Console.WriteLine("RESULT =" + player);
        }
    }
}
