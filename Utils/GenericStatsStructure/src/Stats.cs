﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GenericStatsSystem
{
    /// <summary>
    /// Represents a collection keys and values that can be used as a dictionary and allows some arithmetic operations
    /// </summary>
    /// <typeparam name="TKeyType">type of key</typeparam>
    public class Stats<TKeyType> : IDictionary<TKeyType, float>
    {
        /// <summary>
        /// Statistics container
        /// </summary>
        protected Dictionary<TKeyType, float>  _stats;

        /// <summary>
        /// Flag that tells if collection has changed since last hashcode computation.
        /// </summary>
        protected bool isDirty = true;

        /// <summary>
        /// Collection internal hashCode
        /// </summary>
        protected int _hashCode = 0;

        #region Constructors

        /// <summary>
        /// Generic Constructor
        /// </summary>
        public Stats()
        {
            _stats = new Dictionary<TKeyType, float>();
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        public Stats(Stats<TKeyType> pToCopy)
        {
            this._stats = new Dictionary<TKeyType, float>(pToCopy);
        }

        #endregion

        #region Stats Custom API

        /// <summary>
        /// Checks if <paramref name="pKey"> has been recorded.
        /// </summary>
        /// <param name="pKey">Stat key</param>
        /// <returns>Returns true if the stat is present, otherwise returns false.</returns>
        public bool HasStat(TKeyType pKey)
        {
            return _stats.ContainsKey(pKey);
        }

        /// <summary>
        /// Returns stat value. If stats is missing returns 0.
        /// </summary>
        /// <param name="pKey">Key value to retrieve</param>
        /// <returns>Stat value. If stats is missing returns 0.</returns>
        public float GetStat(TKeyType pKey)
        {
            if (!_stats.ContainsKey(pKey))
                return 0;
            return _stats[pKey];
        }

        /// <summary>
        /// Set stats value. If the <paramref name="pKey"> is missing from current stats, new entry is created.
        /// </summary>
        /// <param name="pKey">Key to add</param>
        /// <param name="pValue">Key value (optional - default is 0)</param>
        public void SetStat(TKeyType pKey, float pValue = 0)
        {
            if (_stats.ContainsKey(pKey))
                _stats[pKey] = pValue;
            else
                _stats.Add(pKey, pValue);
            isDirty = true;
        }

        /// <summary>
        /// Adds new stat. If the <paramref name="pKey"/> is found in current stats, nothing happens.
        /// </summary>
        /// <param name="pKey">Key to add</param>
        /// <param name="pValue">Key value (optional - default is 0)</param>
        public void AddStat(TKeyType pKey, float pValue = 0)
        {
            if (!_stats.ContainsKey(pKey))
            {
                _stats[pKey] = pValue;
                isDirty = true;
            }
        }

        /// <summary>
        /// Removes <paramref name="pKey"> from container
        /// </summary>
        /// <param name="pKey">Key to remove</param>
        public void RemoveStat(TKeyType pKey)
        {
            if (_stats.Count > 0 && _stats.ContainsKey(pKey))
            {
                _stats.Remove(pKey);
                isDirty = true;
            }
        }

        /// <summary>
        /// Adds <paramref name="pValue"/> to all <paramref name="pLeftStats"/> fields.
        /// </summary>
        /// <param name="pValue">value to add</param>
        /// <returns></returns>
        public void Add(float pValue)
        {
            if (_stats.Count > 0)
            {
                ICollection<TKeyType> localKeys = new List<TKeyType>(_stats.Keys);
                foreach (TKeyType key in localKeys)
                    _stats[key] += pValue;
                isDirty = true;
            }
        }

        /// <summary>
        /// Subtracts <paramref name="pValue"/> from all fields.
        /// </summary>
        /// <param name="pValue">value to subtract</param>
        /// <returns></returns>
        public void Sub(float pValue)
        {
            if (_stats.Count > 0)
            {
                ICollection<TKeyType> localKeys = new List<TKeyType>(_stats.Keys);
                foreach (TKeyType key in localKeys)
                    _stats[key] -= pValue;
                isDirty = true;
            }
        }

        /// <summary>
        /// Multiply all fields by <paramref name="pValue"/>.
        /// </summary>
        /// <param name="pValue">value to multiply by</param>
        /// <returns></returns>
        public void Mult(float pValue)
        {
            if (_stats.Count > 0)
            {
                ICollection<TKeyType> localKeys = new List<TKeyType>(_stats.Keys);
                foreach (TKeyType key in localKeys)
                    _stats[key] *= pValue;
                isDirty = true;
            }
        }

        /// <summary>
        /// Divide all fields by <paramref name="pValue"/>.
        /// </summary>
        /// <param name="pValue">value to divide by</param>
        /// <returns></returns>
        public void Div(float pValue)
        {
            if (_stats.Count > 0)
            {
                ICollection<TKeyType> localKeys = new List<TKeyType>(_stats.Keys);
                foreach (TKeyType key in localKeys)
                    _stats[key] /= pValue;
                isDirty = true;
            }
        }

        /// <summary>
        /// Add <paramref name="pStats"/> fields to current fields.
        /// If <paramref name="pIgnoreMissing"/> is set to false, <paramref name="pStats"/> fields missing from this container are added as new entry.
        /// </summary>
        /// <param name="pStats">fields to add</param>
        /// <param name="pIgnoreMissing">flag to ignore missing fields from container</param>
        /// <returns></returns>
        public void Add(Stats<TKeyType> pStats, bool pIgnoreMissing = true)
        {
            if (_stats.Count > 0 && pStats.Count > 0)
            {
                foreach (KeyValuePair<TKeyType, float> kpv in pStats)
                    if (ContainsKey(kpv.Key))
                        SetStat(kpv.Key, this[kpv.Key] + kpv.Value);
                    else if (!pIgnoreMissing)
                        AddStat(kpv.Key, kpv.Value);
                isDirty = true;
            }
        }

        /// <summary>
        /// Subtract <paramref name="pStats"/> fields from current fields.
        /// If <paramref name="pIgnoreMissing"/> is set to false, <paramref name="pStats"/> fields missing from this container are added as new entry (negated value).
        /// </summary>
        /// <param name="pStats">fields to subtract</param>
        /// <param name="pIgnoreMissing">flag to ignore missing fields from container</param>
        /// <returns></returns>
        public void Sub(Stats<TKeyType> pStats, bool pIgnoreMissing = true)
        {
            if (_stats.Count > 0 && pStats.Count > 0)
            {
                foreach (KeyValuePair<TKeyType, float> kpv in pStats)
                    if (ContainsKey(kpv.Key))
                        SetStat(kpv.Key, this[kpv.Key] - kpv.Value);
                    else if (!pIgnoreMissing)
                        AddStat(kpv.Key, -kpv.Value);
            }
        }

        /// <summary>
        /// Returns the multiplication of <paramref name="pStats"/> fields by <paramref name="pStats"/> fields.
        /// If <paramref name="pIgnoreMissing"/> is set to false, <paramref name="pStats"/> fields missing from this container are added as new entry (value = zero).
        /// </summary>
        /// <param name="pStats">fields to multiply</param>
        /// <param name="pIgnoreMissing">flag to ignore missing fields from container</param>
        /// <returns></returns>
        public void Mult(Stats<TKeyType> pStats, bool pIgnoreMissing = true)
        {
            if (_stats.Count > 0 && pStats.Count > 0)
            {
                foreach (KeyValuePair<TKeyType, float> kpv in pStats)
                    if (ContainsKey(kpv.Key))
                        SetStat(kpv.Key, this[kpv.Key] * kpv.Value);
                    else if (!pIgnoreMissing)
                        AddStat(kpv.Key, 0);
                isDirty = true;
            }
        }

        /// <summary>
        /// Returns the division of <paramref name="pStats"/> fields by <paramref name="pStats"/> fields.
        /// If <paramref name="pIgnoreMissing"/> is set to false, <paramref name="pStats"/> fields missing from this container are added as new entry (value = zero).
        /// </summary>
        /// <param name="pStats">fields to divide</param>
        /// <param name="pIgnoreMissing">flag to ignore missing fields from container</param>
        /// <returns></returns>
        public void Div(Stats<TKeyType> pStats, bool pIgnoreMissing = true)
        {
            if (_stats.Count > 0 && pStats.Count > 0)
            {
                foreach (KeyValuePair<TKeyType, float> kpv in pStats)
                    if (ContainsKey(kpv.Key))
                        SetStat(kpv.Key, this[kpv.Key] / kpv.Value);
                    else if (!pIgnoreMissing)
                        AddStat(kpv.Key, 0);
                isDirty = true;
            }
        }

        #endregion

        #region Operators overload

        /// <summary>
        /// Adds <paramref name="pValue"/> to all <paramref name="pLeftStats"/> fields.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator +(Stats<TKeyType> pLeftStats, float pValue)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (TKeyType key in pLeftStats.Keys)
                result[key] += pValue;
            return result;
        }

        /// <summary>
        /// Subtracts <paramref name="pValue"/> from all <paramref name="pLeftStats"/> fields.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator -(Stats<TKeyType> pLeftStats, float pValue)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (TKeyType key in pLeftStats.Keys)
                result[key] -= pValue;
            return result;
        }

        /// <summary>
        /// Multiplies all <paramref name="pLeftStats"/> fields by <paramref name="pValue"/>.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator *(Stats<TKeyType> pLeftStats, float pValue)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (TKeyType key in pLeftStats.Keys)
                result[key] *= pValue;
            return result;
        }

        /// <summary>
        /// Divides all <paramref name="pLeftStats"/> fields by <paramref name="pValue"/>.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator /(Stats<TKeyType> pLeftStats, float pValue)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (TKeyType key in pLeftStats.Keys)
                result[key] /= pValue;
            return result;
        }

        /// <summary>
        /// Returns the result of the addition of <paramref name="pLeftStats"/> fields and <paramref name="pRightStats"/> fields.
        /// If a <paramref name="pRightStats"/> field is missing from <paramref name="pLeftStats"/>, it is added as new entry.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pRightStats"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator +(Stats<TKeyType> pLeftStats, Stats<TKeyType> pRightStats)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (KeyValuePair<TKeyType, float> kpv in pRightStats)
                if (!result.ContainsKey(kpv.Key))
                    result.AddStat(kpv.Key, kpv.Value);
                else
                    result.SetStat(kpv.Key, result[kpv.Key] + kpv.Value);
            return result;
        }

        /// <summary>
        /// Returns the result of the substraction of <paramref name="pLeftStats"/> fields from <paramref name="pRightStats"/> fields.
        /// If a <paramref name="pRightStats"/> field is missing from <paramref name="pLeftStats"/>, it is added as new entry (negative value).
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pRightStats"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator -(Stats<TKeyType> pLeftStats, Stats<TKeyType> pRightStats)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (KeyValuePair<TKeyType, float> kpv in pRightStats)
                if (!result.ContainsKey(kpv.Key))
                    result.AddStat(kpv.Key, -kpv.Value);
                else
                    result.SetStat(kpv.Key, result[kpv.Key] - kpv.Value);
            return result;
        }

        /// <summary>
        /// Returns the multiplication of <paramref name="pLeftStats"/> fields by <paramref name="pRightStats"/> fields.
        /// Fields from <paramref name="pRightStats"/> that are missing from <paramref name="pLeftStats"/> are ignored.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pRightStats"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator *(Stats<TKeyType> pLeftStats, Stats<TKeyType> pRightStats)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (TKeyType key in pRightStats.Keys)
                if (result.ContainsKey(key))
                    result[key] *= pRightStats[key];
            return result;
        }

        /// <summary>
        /// Returns the division of <paramref name="pLeftStats"/> fields by <paramref name="pRightStats"/> fields.
        /// Fields from <paramref name="pRightStats"/> that are missing from <paramref name="pLeftStats"/> are ignored.
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pRightStats"></param>
        /// <returns></returns>
        public static Stats<TKeyType> operator /(Stats<TKeyType> pLeftStats, Stats<TKeyType> pRightStats)
        {
            Stats<TKeyType> result = new Stats<TKeyType>(pLeftStats);
            foreach (TKeyType key in pRightStats.Keys)
                if (result.ContainsKey(key))
                    result[key] /= pRightStats[key];
            return result;
        }

        /// <summary>
        /// Checks equality
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pRightStats"></param>
        /// <returns> True if objetcs are equal. </returns>
        public static bool operator ==(Stats<TKeyType> pLeftStats, Stats<TKeyType> pRightStats)
        {
            return object.ReferenceEquals(pLeftStats, pRightStats) || !object.ReferenceEquals(pLeftStats, null) && pLeftStats.Equals(pRightStats);
        }

        /// <summary>
        /// Checks inequality
        /// </summary>
        /// <param name="pLeftStats"></param>
        /// <param name="pRightStats"></param>
        /// <returns> True if objetcs are different. </returns>
        public static bool operator !=(Stats<TKeyType> pLeftStats, Stats<TKeyType> pRightStats)
        {
            return !( pLeftStats == pRightStats );
        }

        /// <summary>
        /// Gets object hashcode
        /// </summary>
        /// <returns> Object hashcode </returns>
        public override int GetHashCode()
        {
            if (isDirty)
            {
                _hashCode = 0;
                foreach (TKeyType key in Keys.OrderBy(x => x.GetHashCode()))
                    _hashCode += key.GetHashCode() + _stats[key].GetHashCode();
                isDirty = false;
            }
            return _hashCode;
        }

        /// <summary>
        /// Checks equality with <paramref name="pObj">
        /// </summary>
        /// <param name="pObj"></param>
        /// <returns> True if objetcs are equal. </returns>
        public override bool Equals(object pObj)
        {
            Stats<TKeyType> pComparedTo = (Stats<TKeyType>) pObj;

            if (object.ReferenceEquals(pComparedTo, null))
                return false;

            foreach (var kvp in _stats)
            {
                if (!pComparedTo._stats.ContainsKey(kvp.Key))
                    return false;

                if (pComparedTo._stats[kvp.Key] != kvp.Value)
                    return false;
            }

            return true;
        }

        #endregion

        #region IDictionary interface

        public float this[TKeyType key]
        {
            get
            {
                return ( (IDictionary<TKeyType, float>) _stats )[key];
            }

            set
            {
                ( (IDictionary<TKeyType, float>) _stats )[key] = value;
                isDirty = true;
            }
        }

        public void Add(KeyValuePair<TKeyType, float> item)
        {
            ( (IDictionary<TKeyType, float>) _stats ).Add(item);
            isDirty = true;
        }

        public void Add(TKeyType key, float value)
        {
            ( (IDictionary<TKeyType, float>) _stats ).Add(key, value);
            isDirty = true;
        }

        public void Clear()
        {
            ( (IDictionary<TKeyType, float>) _stats ).Clear();
            isDirty = true;
        }

        public bool Remove(KeyValuePair<TKeyType, float> item)
        {
            if (( (IDictionary<TKeyType, float>) _stats ).Remove(item))
            {
                isDirty = true;
                return true;
            }
            return false;
        }

        public bool Remove(TKeyType key)
        {
            if (( (IDictionary<TKeyType, float>) _stats ).Remove(key))
            {
                isDirty = true;
                return true;
            }
            return false;
        }

        public int Count
        {
            get
            {
                return ( (IDictionary<TKeyType, float>) _stats ).Count;
            }
        }

        public bool Contains(KeyValuePair<TKeyType, float> item)
        {
            return ( (IDictionary<TKeyType, float>) _stats ).Contains(item);
        }

        public bool ContainsKey(TKeyType key)
        {
            return ( (IDictionary<TKeyType, float>) _stats ).ContainsKey(key);
        }

        public bool IsReadOnly
        {
            get
            {
                return ( (IDictionary<TKeyType, float>) _stats ).IsReadOnly;
            }
        }

        public ICollection<TKeyType> Keys
        {
            get
            {
                return ( (IDictionary<TKeyType, float>) _stats ).Keys;
            }
        }

        public ICollection<float> Values
        {
            get
            {
                return ( (IDictionary<TKeyType, float>) _stats ).Values;
            }
        }

        public void CopyTo(KeyValuePair<TKeyType, float>[] array, int arrayIndex)
        {
            ( (IDictionary<TKeyType, float>) _stats ).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKeyType, float>> GetEnumerator()
        {
            return ( (IDictionary<TKeyType, float>) _stats ).GetEnumerator();
        }

        public bool TryGetValue(TKeyType key, out float value)
        {
            return ( (IDictionary<TKeyType, float>) _stats ).TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ( (IDictionary<TKeyType, float>) _stats ).GetEnumerator();
        }

        #endregion

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ret = "{\n";
            foreach (TKeyType kvp in Keys)
                ret += " " + kvp + " = " + _stats[kvp] + "\n";
            ret += "}";
            return ret;
        }
    }
}
