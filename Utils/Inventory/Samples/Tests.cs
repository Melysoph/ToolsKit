﻿using GenericStatsSystem;
using System;
using System.Diagnostics;

namespace Inventory.Samples
{
    public enum WeaponType
    {
        Sword,
        Axe
    }

    /// <summary>
    /// Generic entity definition
    /// </summary>
    public class ItemDefinition : IStorableItem
    {
        /// <summary>
        /// Id of the entity (must be unique)
        /// </summary>
        public WeaponType Type;

        uint _weight = 1;

        public uint Weight { set { _weight = value; } }

        /// <summary>
        /// Entity Description
        /// </summary>
        public Stats<string> stats;

        #region IStorableItem interface implementation

        int IStorableItem.HashCode { get { return GetHashCode(); } }

        uint IStorableItem.MaxStackSize { get { return uint.MaxValue; } }

        uint IStorableItem.Weight { get { return _weight; } }

        #endregion

        public override string ToString()
        {
            return Type.ToString() + "\t(" + stats.GetStat("DamageMin") + "-" + stats.GetStat("DamageMax") + ") | " + GetHashCode();
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode() + stats.GetHashCode();
        }
    }

    public class SimpleItem : StorableItemBase
    {
        public string Name;

        public uint Weight = 0;

        public override uint GetWeight()
        {
            return Weight;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return ( Name == ( (SimpleItem) obj ).Name );
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Tests
    {

        private static void ExecuteTest(Func<bool, bool> testName, bool pVerbose = false)
        {
            Console.WriteLine(">>> [" + ( testName.Invoke(pVerbose) ? "OK" : "FAILED" ) + "] " + testName.Method.Name);
        }

        public static void Run()
        {
            ExecuteTest(SlotTest_MaxNumberOfSlot);
            ExecuteTest(QuantityPerstackTest_MaxItemsPerStack);
            ExecuteTest(QuantityTest_MaxOverallItemsCount);
            ExecuteTest(WeightTest_MaxOverallWeight);
            ExecuteTest(WeightTest_MaxWeightPerStack);
            ExecuteTest(AddRemoveTests);
            ExecuteTest(SelectionTest);
            ExecuteTest(LimitedSlotTest);
            ExecuteTest(SortTesting);
        }

        public static bool WeightTest_MaxWeightPerStack(bool pVerbose = false)
        {
            uint maxWeightPerStack = 50;
            SimpleItem Item1 = new SimpleItem() { Name = "Item1", Weight = 1 };
            SimpleItem Item2 = new SimpleItem() { Name = "Item2", Weight = 10 };
            SimpleItem Item3 = new SimpleItem() { Name = "Item3", Weight = 100 };

            Inventory<SimpleItem> inventory = new Inventory<SimpleItem>();
            inventory.MaxWeightPerStack = maxWeightPerStack;

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 30) == 30);   // Weight = 1
            Debug.Assert(inventory.Add(Item1, 30) == 20);   // Weight = 1
            Debug.Assert(inventory.Add(Item2, 2) == 2);     // Weight = 10
            Debug.Assert(inventory.Add(Item2, 30) == 3);    // Weight = 10
            Debug.Assert(inventory.Add(Item3, 30) == 0);    // Weight = 100
            if (pVerbose)
                Console.WriteLine(inventory);

            return true;
        }

        public static bool WeightTest_MaxOverallWeight(bool pVerbose = false)
        {
            uint maxWeight = 50;
            SimpleItem Item1 = new SimpleItem() { Name = "Item1", Weight = 1 };
            SimpleItem Item2 = new SimpleItem() { Name = "Item2", Weight = 10 };
            SimpleItem Item3 = new SimpleItem() { Name = "Item3", Weight = 100 };

            Inventory<SimpleItem> inventory = new Inventory<SimpleItem>();
            inventory.MaxOverallWeight = maxWeight;

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.RejectTooBig;
            Debug.Assert(inventory.Add(Item1, 30) == 30);
            Debug.Assert(inventory.Add(Item1, 30) == 0);
            Debug.Assert(inventory.Add(Item2, 30) == 0);
            Debug.Assert(inventory.Add(Item3, 30) == 0);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 30) == 30);
            Debug.Assert(inventory.Add(Item1, 30) == 20);
            Debug.Assert(inventory.Add(Item2, 30) == 0);
            Debug.Assert(inventory.Add(Item3, 30) == 0);
            if (pVerbose)
                Console.WriteLine(inventory);

            Debug.Assert(inventory.Remove(Item1, 10) == 10);
            Debug.Assert(inventory.Add(Item2, 10) == 1);
            Debug.Assert(inventory.Remove(Item1, 30) == 30);
            Debug.Assert(inventory.Add(Item2, 100) == 3);
            if (pVerbose)
                Console.WriteLine(inventory);

            return true;
        }

        public static bool QuantityTest_MaxOverallItemsCount(bool pVerbose = false)
        {
            uint maxNumber = 50;
            SimpleItem Item1 = new SimpleItem() { Name = "Item1" };
            SimpleItem Item2 = new SimpleItem() { Name = "Item2" };
            SimpleItem Item3 = new SimpleItem() { Name = "Item3" };

            Inventory<SimpleItem> inventory = new Inventory<SimpleItem>();
            inventory.MaxOverallItemsCount = maxNumber;

            inventory.Clear();
            Debug.Assert(inventory.Add(Item1, maxNumber / 2) == maxNumber / 2);
            Debug.Assert(inventory.Add(Item2, maxNumber / 2) == maxNumber / 2);
            Debug.Assert(inventory.Add(Item1, 100) == 0);
            Debug.Assert(inventory.TotalNumberOfItems == maxNumber);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 100) == maxNumber);
            Debug.Assert(inventory.Add(Item2, 25) == 0);
            Debug.Assert(inventory.Add(Item1, 100) == 0);
            Debug.Assert(inventory.TotalNumberOfItems == maxNumber);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.RejectTooBig;
            Debug.Assert(inventory.Add(Item1, 100) == 0);
            Debug.Assert(inventory.Add(Item2, maxNumber / 2) == maxNumber / 2);
            Debug.Assert(inventory.Add(Item1, 100) == 0);
            Debug.Assert(inventory.Add(Item3, 10) == 10);
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 100) == 15);
            Debug.Assert(inventory.TotalNumberOfItems == maxNumber);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 100) == maxNumber);
            Debug.Assert(inventory.Remove(Item1, 20) == 20);
            Debug.Assert(inventory.Add(Item1, 100) == 20);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.Clear();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 100) == maxNumber);
            Debug.Assert(inventory.Remove(Item1, 80) == maxNumber);
            Debug.Assert(inventory.Add(Item1, 100) == maxNumber);
            if (pVerbose)
                Console.WriteLine(inventory);

            return true;
        }

        public static bool QuantityPerstackTest_MaxItemsPerStack(bool pVerbose = false)
        {
            SimpleItem Item1 = new SimpleItem() { Name = "Item1" };
            SimpleItem Item2 = new SimpleItem() { Name = "Item2" };

            Inventory<SimpleItem> inventory = new Inventory<SimpleItem>();
            inventory.MaxItemsPerStack = 2;
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;

            Debug.Assert(inventory.Add(Item1, 1) == 1);
            Debug.Assert(inventory.Add(Item1, 1) == 1);
            Debug.Assert(inventory.Add(Item1, 1) == 0);

            inventory.Clear();
            inventory.MaxItemsPerStack = 2;
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 2) == 2);

            inventory.Clear();
            inventory.MaxItemsPerStack = 2;
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            Debug.Assert(inventory.Add(Item1, 10) == 2);

            inventory.Clear();
            inventory.MaxItemsPerStack = 2;
            inventory.StackAddPolicy = ExceedingPolicyType.RejectTooBig;
            Debug.Assert(inventory.Add(Item1, 10) == 0);

            inventory.Clear();
            inventory.MaxItemsPerStack = 2;
            inventory.StackAddPolicy = ExceedingPolicyType.RejectTooBig;
            Debug.Assert(inventory.Add(Item1, 2) == 2);

            inventory.Clear();
            inventory.MaxItemsPerStack = 2;
            inventory.StackAddPolicy = ExceedingPolicyType.RejectTooBig;
            Debug.Assert(inventory.Add(Item1, 1) == 1);

            return true;
        }

        public static bool SlotTest_MaxNumberOfSlot(bool pVerbose = false)
        {
            SimpleItem a = new SimpleItem() { Name = "Item1" };
            SimpleItem b = new SimpleItem() { Name = "Item2" };
            SimpleItem c = new SimpleItem() { Name = "Item3" };

            Inventory<SimpleItem> inventory = new Inventory<SimpleItem>();
            inventory.MaxNumberOfSlot = 1;

            inventory.Clear();
            Debug.Assert(inventory.Add(a, 1) == 1);
            Debug.Assert(inventory.Add(a, 100) == 100);
            Debug.Assert(inventory.Add(a, 1200) == 1200);
            Debug.Assert(inventory.TotalSlot == 1);
            Debug.Assert(inventory.TotalNumberOfItems == 1301);

            inventory.Clear();
            Debug.Assert(inventory.Add(a, 10) == 10);
            Debug.Assert(inventory.Add(b, 10) == 0);
            Debug.Assert(inventory.Add(a, 100) == 100);
            Debug.Assert(inventory.Add(c, 10000) == 0);
            Debug.Assert(inventory.TotalSlot == 1);
            Debug.Assert(inventory.TotalNumberOfItems == 110);

            inventory.MaxNumberOfSlot = 2;
            Debug.Assert(inventory.Add(c, 10000) == 10000);

            return true;
        }

        public static bool AddRemoveTests(bool pVerbose = false)
        {
            Inventory<ItemDefinition> inventory = new Inventory<ItemDefinition>();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;

            ItemDefinition Axe1 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };
            ItemDefinition Axe2 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };
            ItemDefinition Axe3 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 14 }, { "DamageMax", 15 } } };
            ItemDefinition Axe4 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 18 }, { "DamageMax", 15 } } };
            ItemDefinition Sword1 = new ItemDefinition() { Type = WeaponType.Sword, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };

            Debug.Assert(inventory.Add(Axe1, 1) == 1);
            Debug.Assert(inventory.TotalSlot == 1);

            Debug.Assert(inventory.Add(Axe1, 1) == 1);
            Debug.Assert(inventory.TotalSlot == 1);

            Debug.Assert(inventory.Add(Axe2, 1) == 1); // Axe2 has same Hash as Axe1
            Debug.Assert(inventory.TotalSlot == 1);

            Debug.Assert(inventory.Add(Sword1, 1) == 1);
            Debug.Assert(inventory.TotalSlot == 2);

            Debug.Assert(inventory.Add(Axe3, 1) == 1);
            Debug.Assert(inventory.TotalSlot == 3);

            Debug.Assert(inventory.Add(Axe4, 1) == 1);
            Debug.Assert(inventory.TotalSlot == 4);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectNextStack();
            inventory.SelectNextStack();
            if (pVerbose)
                Console.WriteLine(inventory);

            if (pVerbose)
                Console.WriteLine("RemoveItem : " + Axe3);
            Debug.Assert(inventory.Remove(Axe3, 50) == 1);
            if (pVerbose)
                Console.WriteLine(inventory);

            int index = 0;
            if (pVerbose)
                Console.WriteLine("Removing at " + index + " (" + inventory[index].Item + ")");
            Debug.Assert(inventory.RemoveAt(index, 2) == 2);
            if (pVerbose)
                Console.WriteLine(inventory);
            Debug.Assert(inventory.Add(Sword1, 7) == 7);
            if (pVerbose)
                Console.WriteLine(inventory);

            if (pVerbose)
                Console.WriteLine("RemoveItem : " + Sword1);
            Debug.Assert(inventory.Remove(Sword1, 2) == 2);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectPreviousStack();
            if (pVerbose)
                Console.WriteLine(inventory);
            Debug.Assert(inventory.RemoveAt(0, 2) == 1);
            if (pVerbose)
                Console.WriteLine(inventory);

            return true;
        }

        public static bool SelectionTest(bool pVerbose = false)
        {
            Inventory<ItemDefinition> inventory = new Inventory<ItemDefinition>();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;

            ItemDefinition Axe1 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };
            ItemDefinition Axe2 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };

            ItemDefinition Axe3 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 14 }, { "DamageMax", 15 } } };
            ItemDefinition Axe4 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 18 }, { "DamageMax", 15 } } };
            ItemDefinition Sword1 = new ItemDefinition() { Type = WeaponType.Sword, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };

            inventory.Add(Axe1, 10);
            inventory.Add(Axe1, 4);
            inventory.Add(Axe2, 4);
            inventory.Add(Sword1, 4);
            inventory.Add(Axe3, 3);
            inventory.Add(Axe4, 2);

            Debug.Assert(inventory.SelectedStackIndex == 0);
            inventory.SelectNextStack();
            Debug.Assert(inventory.SelectedStackIndex == 1);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectNextStack();
            Debug.Assert(inventory.SelectedStackIndex == 2);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectNextStack();
            Debug.Assert(inventory.SelectedStackIndex == 3);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectPreviousStack();
            Debug.Assert(inventory.SelectedStackIndex == 2);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectPreviousStack();
            Debug.Assert(inventory.SelectedStackIndex == 1);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectStackByIndex(0);
            Debug.Assert(inventory.SelectedStackIndex == 0);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectStackByIndex(3);
            Debug.Assert(inventory.SelectedStackIndex == 3);
            if (pVerbose)
                Console.WriteLine(inventory);

            inventory.SelectStack(Sword1);
            Debug.Assert(inventory.SelectedStackIndex == 1);
            if (pVerbose)
                Console.WriteLine(inventory);


            Debug.Assert(inventory.GetStackByIndex(0) != null);
            Debug.Assert(( inventory.GetStackByIndex(0).Item as IStorableItem ).HashCode == ( Axe1 as IStorableItem ).HashCode);
            Debug.Assert(( inventory.GetStackByIndex(1).Item as IStorableItem ).HashCode == ( Sword1 as IStorableItem ).HashCode);
            Debug.Assert(inventory.GetStackByItem(Sword1).Item != null);
            Debug.Assert(( inventory.GetStackByItem(Sword1).Item as IStorableItem ).HashCode == ( Sword1 as IStorableItem ).HashCode);
            Debug.Assert(inventory.SelectedStackIndex == 1);

            return true;
        }

        public static bool LimitedSlotTest(bool pVerbose = false)
        {
            Inventory<ItemDefinition> inventory = new Inventory<ItemDefinition>();
            inventory.StackAddPolicy = ExceedingPolicyType.MaxFill;
            inventory.MaxNumberOfSlot = 2;

            ItemDefinition Axe1 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };
            ItemDefinition Axe2 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };
            ItemDefinition Axe3 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 14 }, { "DamageMax", 15 } } };
            ItemDefinition Axe4 = new ItemDefinition() { Type = WeaponType.Axe, stats = new Stats<string>() { { "DamageMin", 18 }, { "DamageMax", 15 } } };
            ItemDefinition Sword1 = new ItemDefinition() { Type = WeaponType.Sword, stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } } };

            Debug.Assert(inventory.Add(Axe1, 10) == 10);
            Debug.Assert(inventory.Add(Axe2, 60) == 60); // Axe2 == Axe1 -- still one slot filled
            Debug.Assert(inventory.Add(Axe3, 10) == 10);
            Debug.Assert(inventory.Add(Axe4, 10) == 0);
            Debug.Assert(inventory.Add(Sword1, 10) == 0);
            if (pVerbose)
                Console.WriteLine(inventory);

            return true;
        }


        private static int SortItemByNameDescending(Stack<ComparableItem> x, Stack<ComparableItem> y)
        {
            return -x.Item.Name.CompareTo(y.Item.Name);
        }

        public class ComparableItem : IStorableItem
        {
            public string Name;

            public uint Weight = 0;

            /// <summary>
            /// Entity Description
            /// </summary>
            public Stats<string> stats;

            #region IStorableItem interface implementation

            int IStorableItem.HashCode { get { return GetHashCode(); } }

            uint IStorableItem.MaxStackSize { get { return uint.MaxValue; } }

            uint IStorableItem.Weight { get { return Weight; } }

            #endregion

            public override string ToString()
            {
                return Name.ToString() + "\t(" + stats.GetStat("DamageMin") + "-" + stats.GetStat("DamageMax") + ")";
            }

            public override int GetHashCode()
            {
                return Name.GetHashCode() + stats.GetHashCode();
            }
        }

        public static bool SortTesting(bool pVerbose = false)
        {
            Inventory<ComparableItem> inventory = new Inventory<ComparableItem>();

            ComparableItem Axe1 = new ComparableItem() { Name = "Axe1", stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } }, Weight = 0 };
            ComparableItem Axe2 = new ComparableItem() { Name = "Axe2", stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } }, Weight = 1 };
            ComparableItem Axe3 = new ComparableItem() { Name = "Axe3", stats = new Stats<string>() { { "DamageMin", 14 }, { "DamageMax", 15 } }, Weight = 220 };
            ComparableItem Axe4 = new ComparableItem() { Name = "Axe4", stats = new Stats<string>() { { "DamageMin", 18 }, { "DamageMax", 15 } }, Weight = 3 };
            ComparableItem Sword1 = new ComparableItem() { Name = "Sword1", stats = new Stats<string>() { { "DamageMin", 12 }, { "DamageMax", 14 } }, Weight = 4 };

            inventory.Add(Axe1, 15);
            inventory.Add(Sword1, 10);
            inventory.Add(Axe3, 20);
            inventory.Add(Axe4, 5);
            inventory.Add(Axe2, 60); // Same HashCode as Axe 1

            try
            {
                if (pVerbose)
                    Console.WriteLine("Before sorting : \n" + inventory);

                inventory.SortStacksByInsertOrderAscending();
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe1.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByInsertOrderAscending \n" + inventory);

                inventory.SortStacksByInsertOrderDescending();
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe2.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByInsertOrderDescending \n" + inventory);

                inventory.SortStacksByQuantityAscending();
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe4.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByQuantityAscending \n" + inventory);

                inventory.SortStacksByQuantityDescending();
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe2.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByQuantityDescending \n" + inventory);

                inventory.SortStacksByWeightAscending();
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe1.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByWeightAscending \n" + inventory);

                inventory.SortStacksByWeightDescending();
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe3.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByWeightDescending \n" + inventory);

                // -- Custom sort

                // SortItemByNameAscending
                inventory.Sort((leftStack, rigthStack) => leftStack.Item.Name.CompareTo(rigthStack.Item.Name));
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Axe1.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByNameAscending \n" + inventory);

                // SortItemByNameDescending
                inventory.Sort((leftStack, rigthStack) => -leftStack.Item.Name.CompareTo(rigthStack.Item.Name));
                Debug.Assert(inventory.GetStackByIndex(0).Item.Name == Sword1.Name);
                if (pVerbose)
                    Console.WriteLine("SortItemByNameDescending \n" + inventory);

            }
            catch (Exception e)
            {
                if (pVerbose)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.InnerException.Message);
                }
                return false;
            }
            return true;
        }
    }
}
