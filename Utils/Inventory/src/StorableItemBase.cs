﻿namespace Inventory
{
    /// <summary>
    /// Default implementation of <see cref="IStorableItem"/> (can be herited).
    /// Explicit interface implementation cannot be overridden in subclasses (IStorableItem methods).
    /// This class is using virtual methods to allow changes in subclasses.
    /// </summary>
    public abstract class StorableItemBase : IStorableItem
    {
        /// <summary>
        /// Returns the item hashchode
        /// </summary>
        int IStorableItem.HashCode { get { return GetHashCode(); } }

        /// <summary>
        /// Returns the maximum size of one stack of this element
        /// </summary>
        uint IStorableItem.MaxStackSize { get { return GetMaxStackSize(); } }

        /// <summary>
        /// Returns the weight of the element
        /// </summary>
        uint IStorableItem.Weight { get { return GetWeight(); } }

        /// <summary>
        /// Returns HashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns Max Stack Size
        /// </summary>
        /// <returns></returns>
        public virtual uint GetMaxStackSize()
        {
            return uint.MaxValue;
        }

        /// <summary>
        /// Returns Weight
        /// </summary>
        /// <returns></returns>
        public virtual uint GetWeight()
        {
            return 1;
        }
    }
}