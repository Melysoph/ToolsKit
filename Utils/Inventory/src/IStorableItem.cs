﻿namespace Inventory
{
    /// <summary>
    /// Interface the <see cref="Inventory<TItemType>"/> can manage
    /// </summary>
    public interface IStorableItem
    {
        /// <summary>
        /// Returns the item hashchode
        /// </summary>
        int HashCode { get; }

        /// <summary>
        /// Returns the maximum size of one stack of this element
        /// </summary>
        uint MaxStackSize { get; }

        /// <summary>
        /// Returns the weight of the element
        /// </summary>
        uint Weight { get; }
    }

}
