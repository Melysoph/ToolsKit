﻿using mattmc3.dotmore.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Inventory
{
    /// <summary>
    /// Class that represents a stack of items and its information (order, quantity, weight)
    /// </summary>
    public class Stack<TItemType> where TItemType : IStorableItem
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pItemDef"></param>
        /// <param name="pInsertionOrderValue"></param>
        /// <param name="pNumber"></param>
        public Stack(ulong pInsertionOrderValue, TItemType pItemDef, ulong pNumber = 1)
        {
            _insertionOrder = pInsertionOrderValue;
            Item = pItemDef;
            _quantity = pNumber;
        }

        /// <summary>
        /// Insertion order
        /// </summary>
        private ulong _insertionOrder;

        /// <summary>
        /// Insertion order
        /// </summary>
        public ulong InsertionOrder { get { return _insertionOrder; } }

        /// <summary>
        /// Number of Item stored
        /// </summary>
        ulong _quantity = 0;

        /// <summary>
        /// Number of item in this stack
        /// </summary>
        public ulong Quantity { get { return _quantity; } set { _quantity = value; } }

        /// <summary>
        /// Total weight of this stack
        /// </summary>
        public ulong TotalWeight { get { return Item.Weight * _quantity; } }

        /// <summary>
        /// List of items ids and their definition
        /// </summary>
        public TItemType Item = default(TItemType);
    }

    /// <summary>
    /// Stack policy on quantity exceeding maximum
    /// </summary>
    public enum ExceedingPolicyType
    {
        MaxFill,       // Fill the quantity
        RejectTooBig   // Reject adding the quantity
    }

    /// <summary>
    /// Collection of <see cref="IStorableItem"/> indexed by <see cref="IStorableItem.HashCode"/> .
    /// </summary>
    public class Inventory<TItemType> : IEnumerable<Stack<TItemType>> where TItemType : IStorableItem
    {
        #region Events

        private Action onInventoryChange_Internal;
        /// <summary>
        /// Event fired on inventory content has changed
        /// </summary>
        public event Action OnInventoryChange
        {
            add { onInventoryChange_Internal += value; }
            remove { onInventoryChange_Internal -= value; }
        }

        private Action onSelectionChange_Internal;
        /// <summary>
        /// Event fired when item selection has changed
        /// </summary>
        public event Action OnSelectionChange
        {
            add { onSelectionChange_Internal += value; }
            remove { onSelectionChange_Internal -= value; }
        }

        #endregion

        /// <summary>
        /// If <paramref name="FillOnMaxed" /> is true, item quantity is set to max stack size, otherwise no item is added.
        /// </summary>
        public ExceedingPolicyType StackAddPolicy = ExceedingPolicyType.MaxFill;

        /// <summary>
        /// Maximum number of different item stacks the inventory can store
        /// </summary>
        public ulong MaxNumberOfSlot = ulong.MaxValue;

        /// <summary>
        /// Maximum number of total items the inventory can store
        /// </summary>
        public ulong MaxOverallItemsCount = ulong.MaxValue;

        /// <summary>
        /// Maximum weight the inventory can store
        /// </summary>
        public ulong MaxOverallWeight = ulong.MaxValue;

        /// <summary>
        /// Maximum weight the inventory can store
        /// </summary>
        public ulong MaxWeightPerStack = ulong.MaxValue;

        /// <summary>
        /// Maximum items a stack can contains
        /// </summary>
        public ulong MaxItemsPerStack = ulong.MaxValue;

        /// <summary>
        /// Stored item stacks accessible by item hash
        /// </summary>
        private OrderedDictionary<int, Stack<TItemType>> _storedStacks = new OrderedDictionary<int, Stack<TItemType>>();

        /// <summary>
        /// Number of unique item stack entry (hash)
        /// </summary>
        private ulong _uniqueItemCount = 0;

        /// <summary>
        /// Number of item stacks stored (quantities sum)
        /// </summary>
        private ulong _numberOfStacks = 0;

        /// <summary>
        /// Total weight carried in the inventory (weights summed)
        /// </summary>
        private ulong _totalWeight = 0;

        /// <summary>
        /// Insertion index. Used to keep track of stack insert order
        /// </summary>
        private static ulong insertCount = 0;

        /// <summary>
        /// Get next insertion index
        /// </summary>
        /// <returns></returns>
        private static ulong GetNextIndex()
        {
            insertCount += 1;
            return insertCount;
        }

        #region Access API

        /// <summary>
        /// Adds a <paramref name="pQuantity" /> of <paramref name="pItem" /> to inventory regarding to containers limits and adding policy.
        /// Returns the number of items added in stack.
        /// </summary>
        /// <param name="pItem"> item definition </param>
        /// <param name="pQuantity"> number of item to add </param>
        /// <returns> the number of items added in stack </returns>
        public ulong Add(TItemType pItem, ulong pQuantity = 1)
        {
            if (pItem == null || TotalNumberOfItems >= MaxOverallItemsCount || ( TotalWeight >= MaxOverallWeight && pItem.Weight > 0 ))
                return 0;

            if (_storedStacks.ContainsKey(pItem.HashCode))
            {
                Stack<TItemType> res;
                _storedStacks.TryGetValue(pItem.HashCode, out res);

                pQuantity = GetMaxQuantity(pItem, pQuantity, res.Quantity, res.TotalWeight);
                res.Quantity += pQuantity;
            }
            else
            {
                if (TotalSlot >= MaxNumberOfSlot)
                    return 0;

                pQuantity = GetMaxQuantity(pItem, pQuantity, 0, 0);
                _storedStacks.Add(pItem.HashCode, new Stack<TItemType>(GetNextIndex(), pItem, pQuantity));
                _uniqueItemCount += 1;

                // Auto-Select if its the first item added
                if (_selectedStackIndex < 0)
                {
                    _selectedStackIndex = 0;
                    if (onSelectionChange_Internal != null)
                        onSelectionChange_Internal.Invoke();
                }
            }

            // Fire Change Event
            if (onInventoryChange_Internal != null)
                onInventoryChange_Internal.Invoke();

            _numberOfStacks += pQuantity;
            _totalWeight += pQuantity * pItem.Weight;

            return pQuantity;
        }

        /// <summary>
        /// Removes <paramref name="pQuantity"/> of item at index <param name="pIndex"/> from inventory
        /// </summary>
        /// <param name="pIndex"> Item to remove </param>
        /// <param name="pQuantity"> Quantity of item to remove, removes all if quantity is lower than 1 </param>
        /// <returns></returns>
        public ulong RemoveAt(int pIndex, ulong pQuantity = 0)
        {
            if (_storedStacks.Count <= pIndex)
                return 0;

            ulong removedQuantity = pQuantity;

            // Remove item
            if (pQuantity <= 0 || (int) _storedStacks[pIndex].Quantity - (int) pQuantity <= 0)
            {
                if (( pIndex == _storedStacks.Count - 1 && pIndex == SelectedStackIndex )
                    || pIndex < SelectedStackIndex)
                    SelectPreviousStack();

                removedQuantity = _storedStacks[pIndex].Quantity;
                _totalWeight -= _storedStacks[pIndex].TotalWeight;

                _storedStacks.RemoveAt(pIndex);
                _uniqueItemCount -= 1;
            }
            else
            {
                _storedStacks[pIndex].Quantity -= pQuantity;
                _totalWeight -= removedQuantity * _storedStacks[pIndex].Item.Weight;
            }

            // Fire Change Event
            if (onInventoryChange_Internal != null)
                onInventoryChange_Internal.Invoke();

            _numberOfStacks -= removedQuantity;

            return removedQuantity;
        }

        /// <summary>
        /// Removes <paramref name="pQuantity"/> of <paramref name="pItem"/>s from inventory.
        /// </summary>
        /// <param name="pItem"> Item to remove </param>
        /// <param name="pQuantity"> Quantity of item to remove, removes all if quantity is lower than 1 </param>
        /// <returns> Quantity of item removed </returns>
        public ulong Remove(TItemType pItem, ulong pQuantity = 0)
        {
            if (_storedStacks.Count <= 0)
                return 0;

            Stack<TItemType> res;
            _storedStacks.TryGetValue(pItem.HashCode, out res);

            if (res == null)
                return 0;

            int pIndex = IndexOf(pItem);
            ulong removedQuantity = pQuantity;

            // Remove item
            if (pQuantity <= 0 || (int) res.Quantity - (int) pQuantity <= 0)
            {
                if (( pIndex == _storedStacks.Count - 1 && pIndex == SelectedStackIndex )
                    || pIndex < SelectedStackIndex)
                    SelectPreviousStack();

                removedQuantity = res.Quantity;
                _totalWeight -= _storedStacks[pIndex].TotalWeight;

                _storedStacks.Remove(pItem.HashCode);
                _uniqueItemCount -= 1;
            }
            else
            {
                res.Quantity -= pQuantity;
                _totalWeight -= removedQuantity * _storedStacks[pIndex].Item.Weight;
            }

            // Fire Change Event
            if (onInventoryChange_Internal != null)
                onInventoryChange_Internal.Invoke();

            _numberOfStacks -= removedQuantity;

            return removedQuantity;
        }

        /// <summary>
        /// Returns index of <paramref name="pItem"/> in list
        /// </summary>
        /// <returns></returns>
        public int IndexOf(TItemType pItem)
        {
            return _storedStacks.IndexOf(pItem.HashCode);
        }

        /// <summary>
        /// Removes all stacks from this inventory
        /// </summary>
        public void Clear()
        {
            _storedStacks.Clear();
            SelectedStackIndex = 0;
            _uniqueItemCount = 0;
            _numberOfStacks = 0;
            _totalWeight = 0;
        }

        /// <summary>
        /// Gets or sets the value at the specified index.
        /// </summary>
        /// <param name="pIndex">The index of the value to get or set.</param>
        public Stack<TItemType> this[int pIndex]
        {
            get
            {
                return _storedStacks[pIndex];
            }
            set
            {
                _storedStacks[pIndex] = value;
            }
        }

        /// <summary>
        /// Gets or sets the value at the specified index.
        /// </summary>
        /// <param name="pIndex">The index of the value to get or set.</param>
        public Stack<TItemType> ElementAt(int pIndex)
        {
            return _storedStacks[pIndex];
        }

        /// <summary>
        /// Return the number of unique item entry (uses hash)
        /// </summary>
        public ulong TotalSlot { get { return _uniqueItemCount; } }

        /// <summary>
        /// Return the number of items stored (summed quantities)
        /// </summary>
        public ulong TotalNumberOfItems { get { return _numberOfStacks; } }

        /// <summary>
        /// Return the weight carried in the inventory (summed weights)
        /// </summary>
        public ulong TotalWeight { get { return _totalWeight; } }

        #endregion

        #region Sorting

        /// <summary>
        /// Sort item stacks using default comparer
        /// </summary>
        public void Sort()
        {
            SortStacksByInsertOrderAscending();
        }

        /// <summary>
        /// Sort item stacks using specified comparer
        /// </summary>
        /// <param name="pComparison"></param>
        public void Sort(Comparison<Stack<TItemType>> pComparison)
        {
            _storedStacks.SortValues(pComparison);
        }

        /// <summary>
        /// Sort item stacks using specified comparer
        /// </summary>
        /// <param name="pComparer"></param>
        public void Sort(IComparer<Stack<TItemType>> pComparer)
        {
            _storedStacks.SortValues(pComparer);
        }

        /// <summary>
        /// Sort <paramref name="pCount"/> numer of item stacks, starting from index <paramref name="pIndex"/>, using specified comparer <paramref name="pComparer"/>
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pCount"></param>
        /// <param name="pComparer"></param>
        public void Sort(int pIndex, int pCount, IComparer<Stack<TItemType>> pComparer)
        {
            _storedStacks.SortValues(pComparer);
        }

        /// <summary>
        /// SortStacksByInsertOrderAscending
        /// </summary>
        public void SortStacksByInsertOrderAscending() { Sort((x, y) => x.InsertionOrder.CompareTo(y.InsertionOrder)); }

        /// <summary>
        /// SortStacksByInsertOrderDescending
        /// </summary>
        public void SortStacksByInsertOrderDescending() { Sort((x, y) => -x.InsertionOrder.CompareTo(y.InsertionOrder)); }

        /// <summary>
        /// SortStacksByQuantityAscending
        /// </summary>
        public void SortStacksByQuantityAscending() { Sort((x, y) => x.Quantity.CompareTo(y.Quantity)); }

        /// <summary>
        /// SortStacksByQuantityDescending
        /// </summary>
        public void SortStacksByQuantityDescending() { Sort((x, y) => -x.Quantity.CompareTo(y.Quantity)); }

        /// <summary>
        /// SortStacksByWeightAscending
        /// </summary>
        public void SortStacksByWeightAscending() { Sort((leftStack, rigthStack) => leftStack.TotalWeight.CompareTo(rigthStack.TotalWeight)); }

        /// <summary>
        /// SortStacksByWeightDescending
        /// </summary>
        public void SortStacksByWeightDescending() { Sort((leftStack, rigthStack) => -leftStack.TotalWeight.CompareTo(rigthStack.TotalWeight)); }

        #endregion

        /// <summary>
        /// Returns the maximum addable quantity regarding inventory limits (in quantities and weights)
        /// </summary>
        /// <param name="pItem"></param>
        /// <param name="pQuantity"></param>
        /// <param name="initialQuantity"></param>
        /// <param name="initialWeight"></param>
        /// <returns></returns>
        private ulong GetMaxQuantity(TItemType pItem, ulong pQuantity, ulong initialQuantity, ulong initialWeight)
        {
            // Trim pQuantity value regarding quantity
            ulong MaxAddableQuantity = (ulong) Math.Min(MaxOverallItemsCount - TotalNumberOfItems, Math.Min(pItem.MaxStackSize, MaxItemsPerStack) - initialQuantity);
            if (pQuantity > MaxAddableQuantity && StackAddPolicy == ExceedingPolicyType.RejectTooBig)
                return 0;
            pQuantity = Math.Min(pQuantity, MaxAddableQuantity);

            // Trim pQuantity value regarding weight
            ulong MaxWeightToAdd = (ulong) Math.Min(MaxOverallWeight - TotalWeight, MaxWeightPerStack - initialWeight);
            ulong addedWeigth = pQuantity * pItem.Weight;
            if (addedWeigth > MaxWeightToAdd && StackAddPolicy == ExceedingPolicyType.RejectTooBig)
                return 0;
            if (addedWeigth > MaxWeightToAdd)
                pQuantity = MaxWeightToAdd / pItem.Weight;

            return pQuantity;
        }

        #region Item Selection API

        /// <summary>
        /// Index of a selected item stack
        /// </summary>
        private int _selectedStackIndex = -1;

        /// <summary>
        /// Index of a selected item stack
        /// </summary>
        public int SelectedStackIndex { get { return _selectedStackIndex; } private set { _selectedStackIndex = value; } }

        /// <summary>
        /// Has the player a selected item stack in inventory ?
        /// </summary>
        public bool HasStackSelected
        {
            get
            {
                if (SelectedStackIndex >= 0 && SelectedStackIndex < _storedStacks.Count)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Returns selected item stack
        /// </summary>
        /// <returns></returns>
        public Stack<TItemType> GetSelectedStack()
        {
            if (!HasStackSelected)
                return null;
            return _storedStacks[_selectedStackIndex];
        }

        /// <summary>
        /// Returns the item stack at specified index
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        public Stack<TItemType> GetStackByIndex(int pIndex)
        {
            return _storedStacks.ElementAt(pIndex).Value;
        }

        /// <summary>
        /// Returns the corresponding item stack
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns> item stack of exists, null otherwise </returns>
        public Stack<TItemType> GetStackByItem(TItemType pItem)
        {
            if (pItem == null || !_storedStacks.ContainsKey(pItem.HashCode))
                return null;
            return _storedStacks[_storedStacks.Keys.ToList().IndexOf(pItem.HashCode)];
        }

        /// <summary>
        /// Select next item stack in list
        /// </summary>
        public void SelectNextStack()
        {
            if (_storedStacks.Count == 0)
                return;

            _selectedStackIndex = ( _selectedStackIndex + 1 ) % _storedStacks.Count;

            if (onSelectionChange_Internal != null)
                onSelectionChange_Internal.Invoke();
        }

        /// <summary>
        /// Select previous item stack in list
        /// </summary>
        public void SelectPreviousStack()
        {
            if (_storedStacks.Count == 0)
                return;

            _selectedStackIndex--;
            if (_selectedStackIndex < 0)
                _selectedStackIndex = _storedStacks.Count - 1;

            if (onSelectionChange_Internal != null)
                onSelectionChange_Internal.Invoke();
        }

        /// <summary>
        /// Select item stack by index
        /// </summary>
        /// <param name="pIndex"></param>
        public void SelectStackByIndex(int pIndex)
        {
            if (_storedStacks.Count == 0 || pIndex < 0 || pIndex >= _storedStacks.Count)
                return;
            _selectedStackIndex = pIndex;

            if (onSelectionChange_Internal != null)
                onSelectionChange_Internal.Invoke();
        }

        /// <summary>
        /// Select item stack by id
        /// </summary>
        /// <param name="pIndex"></param>
        public void SelectStack(TItemType pItem)
        {
            if (!_storedStacks.ContainsKey(pItem.HashCode))
                return;
            _selectedStackIndex = _storedStacks.Values.ToList().IndexOf(_storedStacks.GetValue(pItem.HashCode));

            if (onSelectionChange_Internal != null)
                onSelectionChange_Internal.Invoke();
        }

        #endregion

        #region IEnumerable interface implementation

        /// <summary>
        /// Get Enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Stack<TItemType>> GetEnumerator()
        {
            return _storedStacks.Values.GetEnumerator();
        }

        /// <summary>
        /// Get Enumerator
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        /// <summary>
        /// Returns string representation of class
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ret = "Inventory ( Total Stacks : "
                + TotalSlot + " | Total Items in Stacks : "
                + TotalNumberOfItems + " | Total Weight : "
                + TotalWeight + ") : \n";
            foreach (KeyValuePair<int, Stack<TItemType>> item in _storedStacks)
                ret += item.Value.Item.ToString() + "\t: "
                    + item.Value.Quantity + "\t: "
                    + item.Value.TotalWeight + "\t: "
                    + item.Value.InsertionOrder
                    + ( GetSelectedStack().Item.HashCode == item.Value.Item.HashCode ? "*" : "" )
                    + "\n";
            return ret;
        }
    }
}