﻿using Inventory.Samples;

namespace Inventory
{
    class Program
    {
        static void Main(string[] args)
        {
            Tests.Run();
        }
    }
}
